package com.hyl.test.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * RabbitMQ 队列配置
 * @author hyl
 * @date 2018-4-23
 */
@Component
public class RabbitConfig {

    @Bean
    public Queue hello(){
        return new Queue("hello");
    }

    @Bean
    public Queue work(){
        return new Queue("work");
    }

    @Bean
    public Queue fanoutA(){
        return new Queue("fanout.a");
    }

    @Bean
    public Queue fanoutB(){
        return new Queue("fanout.b");
    }


    /**
     * 新建一个交换机
     * @return 返回一个交换机
     */
    @Bean
    FanoutExchange fanoutExchange(){
        return new FanoutExchange("fanout");
    }

    @Bean
    Binding bindingExchangeA(Queue fanoutA,FanoutExchange fanoutExchange){
        return BindingBuilder.bind(fanoutA).to(fanoutExchange);
    }

    /**
     * 将队列绑定至交换机上
     * @param fanoutB 绑定至 fanoutExchange 上的队列bean
     * @param fanoutExchange
     * @return
     */
    @Bean
    Binding bindingExchangeB(Queue fanoutB,FanoutExchange fanoutExchange){
        return BindingBuilder.bind(fanoutB).to(fanoutExchange);
    }


    // 下面的是主题队列

    @Bean
    public Queue topicB(){
        return new Queue("topic.b");
    }

    @Bean
    public Queue topicA(){
        return new Queue("topic.a");
    }

    @Bean
    TopicExchange topicExchange(){
        return new TopicExchange("topic");
    }

    /**
     * 完整的匹配topic.message,才能接受
     * @param topicA
     * @param topicExchange
     * @return
     */
    @Bean
    Binding bindingTopExchange(Queue topicA,TopicExchange topicExchange){
        return BindingBuilder.bind(topicA).to(topicExchange).with("topic.message");
    }

    @Bean
    Binding bindingTopExchanges(Queue topicB,TopicExchange topicExchange){
        return BindingBuilder.bind(topicB).to(topicExchange).with("topic.#");
    }
}
