package com.hyl.test.rabbitmq.listen;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 对队列 hello进行监听
 * @author hyl
 * @date 2018-4-23
 */
@Component
@RabbitListener(queues="hello")
public class HelloListener {

    @RabbitHandler
    public void process(String message){
        System.out.println(message);
    }
}
