package com.hyl.test.rabbitmq.listen;

import com.hyl.test.aop.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 主题模式
 * @author hyl
 * @date 2018-4-23
 */
@Component
@RabbitListener(queues = "topic.b")
public class TopicListenerB {

    @RabbitHandler
    @Logger("topic 测试")
    public void process(String message){
        System.out.println(message);
    }
}
