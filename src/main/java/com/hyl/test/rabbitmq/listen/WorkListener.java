package com.hyl.test.rabbitmq.listen;

import com.hyl.test.aop.Logger;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 工作模式 其中一个消费者
 * @author hyl
 * @date 2018-4-23
 */
@Component
@RabbitListener(queues = "work")
public class WorkListener {

    @RabbitHandler
    @Logger
    public void process(String work){
        System.out.println(work);
    }
}
