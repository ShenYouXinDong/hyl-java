package com.hyl.test.thread;

/**
 * ThreadLocal 没试
 * @author hyl
 * @date 2018-4-21
 */
public class ThreadLocalString {

    ThreadLocal<String> stringThreadLocal=new ThreadLocal<String>(){
        @Override
        protected String initialValue() {
            // 如若不重写，没有先se，直接get的话，运行时会报空批针异常
            return Thread.currentThread().getName();
        }
    };
}
