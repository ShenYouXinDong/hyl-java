package com.hyl.test.thread;


import java.util.concurrent.Semaphore;

/**
 * Semaphore 测试
 * @author hyl
 * @date 2018-4-20
 */
public class SemaphoreRunnable implements Runnable{

    private Semaphore semaphore;

    public SemaphoreRunnable(Semaphore semaphore){
        this.semaphore=semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            Thread.sleep(2000);
            System.out.println("两秒后执行一下:"+ Thread.currentThread().getId()+"name:"+Thread.currentThread().getName());

            semaphore.release();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
