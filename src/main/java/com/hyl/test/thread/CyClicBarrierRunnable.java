package com.hyl.test.thread;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @author hyl
 * @date 2018-4-20
 */
public class CyClicBarrierRunnable implements Runnable{

    private CyclicBarrier cyclicBarrier;

    public CyClicBarrierRunnable(){}

    public CyClicBarrierRunnable(CyclicBarrier cyclicBarrier){
        this.cyclicBarrier=cyclicBarrier;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
            System.out.println("CyClicBarrierRunnable 执行");
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
