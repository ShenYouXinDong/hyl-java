package com.hyl.test.thread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 多线程测试
 * @author hyl
 * @date 2018-4-20
 */
public class Test {

    @org.junit.Test
    public void test(){
        CyclicBarrier cyclicBarrier=new CyclicBarrier(2);

        CyClicBarrierRunnable thread=new CyClicBarrierRunnable(cyclicBarrier);

        Thread t=new Thread(thread);
        t.start();
        try {
            Thread.sleep(6000);
            // 模拟一个线程执行
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }

        System.out.println("两个线程执行完成后，再执行这个打印");
    }

    @org.junit.Test
    public void test2(){
        Semaphore semaphore=new Semaphore(2);
        SemaphoreRunnable t=new SemaphoreRunnable(semaphore);
        SemaphoreRunnable t2=new SemaphoreRunnable(semaphore);

        Thread thread=new Thread(t2);
        thread.start();
        Thread thread2=new Thread(t2);
        thread2.start();


        Thread thread3=new Thread(t2);
        thread3.start();

        Thread thread4=new Thread(t2);
        thread4.start();

        Thread thread5=new Thread(t2);
        thread5.start();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void test3(){
        Exchanger<String> stringExchanger=new Exchanger<>();
        ExecutorService threadPool=Executors.newFixedThreadPool(2);

        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                String a="xxxx";
                try {
                    stringExchanger.exchange(a);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                String b="yyyy";
                try {
                    String a=stringExchanger.exchange("ccc");
                    System.out.println(a);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        threadPool.shutdown();
    }

    @org.junit.Test
    public void test4(){

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                ThreadLocalString threadLocal=new ThreadLocalString();
                threadLocal.stringThreadLocal.set("神一般的测试");
                System.out.println(threadLocal.stringThreadLocal.get());
            }
        });

        thread.start();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void test6(){
        List list = new ArrayList();
    }
}
