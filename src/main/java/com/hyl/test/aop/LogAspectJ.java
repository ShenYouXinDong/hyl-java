package com.hyl.test.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @author hyl
 * @date 2018-4-22
 */
@Aspect
@Component
public class LogAspectJ {

    private Logger logger= LoggerFactory.getLogger(LogAspectJ.class);

    @Pointcut("execution(* com.hyl.test..*.*(..))")
    public void beforeLog(){
    }

    @Around("execution(* com.hyl.test..*.*(..)) && @annotation(log)")
    public Object afterLog(ProceedingJoinPoint pjd, com.hyl.test.aop.Logger log){
        String logId=UUID.randomUUID().toString();
        logger.info( "["+ logId +"] around使用之前");
        Object result=null;
        logger.info("["+ logId +"] 方法开始执行! 方法名:"+pjd.getSignature().getName()+" target:"+pjd.getTarget()+"注解值:"+log.value());
        try {
           result=pjd.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        logger.info("["+ logId +"] around使用之后");
        return result;
    }

//    @Before("beforeLog()")
//    public void before(){
//        logger.info("方法之前执行!");
//    }
//
//    @After("execution(* com.hyl.test..*.*(..)) && @annotation(com.hyl.test.aop.Logger)")
//    public void after(){
//        logger.info("方法之后执行!");
//    }

}
