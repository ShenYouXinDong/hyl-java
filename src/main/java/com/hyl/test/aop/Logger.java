package com.hyl.test.aop;

import java.lang.annotation.*;

/**
 * Logger 注解
 * @author hyl
 * @date 2018-4-22
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface Logger {

     String value() default "";
}
