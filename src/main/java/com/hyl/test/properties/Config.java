package com.hyl.test.properties;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * classpath:config.properties 映射实体类
 * https://docs.spring.io/spring-boot/docs/1.5.10.RELEASE/reference/html/configuration-metadata.html#configuration-metadata-annotation-processor 官网介绍
 * @author hyl
 * @date 2018-5-8
 */
@Component
@ConfigurationProperties(prefix = "config")
public class Config {
//    @NotEmpty
    private String name;

    private int port;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
