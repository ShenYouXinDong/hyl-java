package com.hyl.test.controller;

import com.hyl.test.aop.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * loggerAspectJ 测试
 * @author hyl
 * @date 2018-4-22
 */
@RestController
public class LoggerController {

    @GetMapping("/say")
    @Logger("神一般的测试")
    public String say(){
        System.out.println("controller say done");
        return "xxxxx";
    }
}
