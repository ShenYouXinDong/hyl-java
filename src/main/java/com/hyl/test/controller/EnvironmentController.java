package com.hyl.test.controller;

import com.hyl.test.properties.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Environment 读取 application.yml 的属性
 * @author hyl
 * @date 2018=5-8
 */
@RestController
public class EnvironmentController {

    private Logger logger = LoggerFactory.getLogger(EnvironmentController.class);

    @Autowired
    private Environment environment;

    @Autowired
    private Config config;

    @GetMapping("/env")
    public Config environment(){
        logger.info(environment.getProperty("spring.application.name"));
        return config;
    }
}
