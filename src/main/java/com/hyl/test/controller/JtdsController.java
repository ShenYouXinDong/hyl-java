package com.hyl.test.controller;

import com.hyl.test.jtds.DBUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jtds")
public class JtdsController {

    @GetMapping("/test")
    public void test(){
        DBUtils.QuerySQL();
    }
}
