package com.hyl.test.controller;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * RabbitMQ 测试
 * @author hyl
 * @date 2018-4-23
 */
@RestController
public class RabbitmqController {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @GetMapping("/hello")
    public void send(){
        this.rabbitTemplate.convertAndSend("hello","神一般的测试 ");
    }

    @GetMapping
    public void work(){
        this.rabbitTemplate.convertAndSend("work","神一般的测试 ");
//        for (int i=0;i<6;i++){
//            this.rabbitTemplate.convertAndSend("work","神一般的测试 "+i);
//        }
    }

    @GetMapping("/fanout")
    public void fanout(){
        this.rabbitTemplate.convertAndSend("fanout","","神一般的测试 fanout");
    }

    @GetMapping("/topic")
    public void topic(){
        this.rabbitTemplate.convertAndSend("topic","topic.1","神一般的测试 topic.1");
        this.rabbitTemplate.convertAndSend("topic","topic.message","神一般的测试 topic.message");
    }
}
