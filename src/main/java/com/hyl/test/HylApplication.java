package com.hyl.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 启动类
 * @author hyl
 * @date 2018-4-22
 */
@SpringBootApplication
@EnableAspectJAutoProxy
public class HylApplication {

    public static void main(String []args){
        SpringApplication.run(HylApplication.class,args);
    }
}
