##### 1.初识 RabbitMQ

- 1.启动RabbitMQ服务
    ````
    rabbitmq-service stop    rabbitmq-server.bat  
    ````
 - 2.添加maven配置
    ````
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-amqp</artifactId>
    </dependency>
    ````
 - 3.application.yml文件配置
    ````
    spring:
      application:
        name: hyl-java
      rabbitmq:
        host: 10.30.90.136
        port: 5672
        username: admin
        password: 123456
    ````
   
 ***
 
 **代码在rabbitmq包下**
 
 
 
 > RabbitMQ的几种模式
 
 ![几种模式](../../uploads/images/RabbitMQ/1138295-2017121417331610xxxxxxxx.png)

> 1.hello world

 ![hello world](../../uploads/images/RabbitMQ/1138295-20171213214238410-380279082.png)</br>
 
P代表生产者，C代表消费者，红色代码消息队列。P将消息发送到消息队列，C对消息进行处理。

> 2.工作模式(竞争)

![hello world](../../uploads/images/RabbitMQ/1138295-20171214160419201-2114146978.png)</br>
一个消息产生者，多个消息的消费者。竞争抢消息,基本上平均分配  

> 3.发布订阅模式

![hello world](../../uploads/images/RabbitMQ/1138295-20171214162640404-2100469618.png)</br>
生产者不再是将消息直接发送到队列，而是发送到X交换机，然后由交换机发送给两个(多个)队列,两个消费者各自监听一个队列，
来消费消息。这种方式实现同一个消息被多个消费者消费

fanout 英[fænaʊt] 分列（帐户）

> 4:路由模式

![hello world](../../uploads/images/RabbitMQ/1138295-20171214170430873-2060627491.png)</br>
需要将一个队列绑定到交换机上，要求该消息与一个特定的路由键完全匹配，这是一个完整的匹配。
在发布订阅模式下做些修改

> 5.主题模式

发送端不只按固定的routing key发送消息，而是按字符串匹配发送，接收端同样如此
符号#匹配一个或多个词，符号*匹配不多不少一个词。
![hello world](../../uploads/images/RabbitMQ/1138295-20171214170908217-1236800740.png)</br>
4/5两者模式很相似



