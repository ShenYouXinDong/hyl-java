##### 1.初识 Exchanger
> Exchanger（交换者）是一个用于线程间协作的工具类。Exchanger用于进行线程间的数据交换。它提供一个同步点，在这个同步点两个线程可以交换彼此的数据

两个线程通过exchange方法交换数据， 如果第一个线程先执行exchange方法，
它会一直等待第二个线程也执行exchange，当两个线程都到达同步点时，
这两个线程就可以交换数据，将本线程生产出来的数据传递给对方

应用场景:Exchanger 可以用于校对工作。需要将纸制银流通过人工的方式录入成电子银行流水，为了避免错误，采用AB岗两人进行录入，录入到Excel之后，系统需要加载这两个Excel，并对这两个Excel数据进行校对，看看是否录入的一致

````
@org.junit.Test
public void test3(){
    Exchanger<String> stringExchanger=new Exchanger<>();
    ExecutorService threadPool=Executors.newFixedThreadPool(2);

    threadPool.execute(new Runnable() {
        @Override
        public void run() {
            String a="xxxx";
            try {
                stringExchanger.exchange(a);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });

    threadPool.execute(new Runnable() {
        @Override
        public void run() {
            String b="yyyy";
            try {
                String a=stringExchanger.exchange("ccc");
                System.out.println(a);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });

    threadPool.shutdown();
}
````

**如果两个线程有一个没有到达exchange方法，则会一直等待,如果担心有特殊情况发生，避免一直等待，可以使用exchange(V x, long timeout, TimeUnit unit)设置最大等待时长**