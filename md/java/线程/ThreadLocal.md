##### 1.初识 ThreadLocal
> ThreadLocal的应用场景:最常见的ThreadLocal使用场景为 用来解决 数据库连接、Session管理等。

常见的几个方法：
- public T get() { }
    - get()方法是用来获取ThreadLocal在当前线程中保存的变量副本
- public void set(T value) { }
    - set()用来设置当前线程中变量的副本
- public void remove() { }
    - remove()用来移除当前线程中变量的副本
- protected T initialValue() { }
    - initialValue()是一个protected方法，一般是用来在使用时进行重写的，它是一个延迟加载方法

代码示例
````
@org.junit.Test
public void test4(){
    Thread thread=new Thread(new Runnable() {
        @Override
        public void run() {
            ThreadLocalString threadLocal=new ThreadLocalString();
            threadLocal.stringThreadLocal.set("神一般的测试");
            System.out.println(threadLocal.stringThreadLocal.get());
        }
    });

    thread.start();

    try {
        Thread.sleep(2000);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
}
````