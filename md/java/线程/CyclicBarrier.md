##### 1.初识
> 栅栏允许两个或者多个线程在某个集合点同步。
1. 当一个线程到达集合点时，它将调用await()方法等待其它的线程。
2. 线程调用await()方法后，CyclicBarrier将阻塞这个线程并将它置入休眠状态等待其它线程的到来。
3. 等最后一个线程调用await()方法时，CyclicBarrier将唤醒所有等待的线程然后这些线程将继续执行。
4. CyclicBarrier可以传入另一个Runnable对象作为初始化参数。当所有的线程都到达集合点后，CyclicBarrier类将Runnable对象作为线程执行。
> **应用场景**s:可看成是个障碍，所有的线程必须到齐后才能一起通过这个障碍

代码例子
````java
/**
 * @author hyl
 * @date 2018-4-20
 */
public class CyClicBarrierRunnable implements Runnable{

    private CyclicBrrier cyclicBarrier;

    public CyClicBarrierRunnable(){}

    public CyClicBarrierRunnable(CyclicBarrier cyclicBarrier){
        this.cyclicBarrier=cyclicBarrier;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
            System.out.println("CyClicBarrierRunnable 执行");
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }
}
````

````java
/**
 * 多线程测试
 * @author hyl
 * @date 2018-4-20
 */
public class Test {

    @org.junit.Test
    public void test(){
        CyclicBarrier cyclicBarrier=new CyclicBarrier(2);

        CyClicBarrierRunnable thread=new CyClicBarrierRunnable(cyclicBarrier);

        Thread t=new Thread(thread);
        t.start();
        try {
            Thread.sleep(6000);
            // 模拟一个线程执行
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }

        System.out.println("两个线程执行完成后，再执行这个打印");
    }
}
````