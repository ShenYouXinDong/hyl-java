### 基本功
##### 1.面向对象的特征
继承 抽象 多态
##### 2.final,finally,finalize的区别
- final:修饰变量则值不可以修改，修饰方法则方法不可被重写，修饰类则类不可以被继承
- finally:用于捕获异常模块中，不管是否发生异常，finally语句块中一定执行
- finalize:Object类中的方法，子类重写该方法，可以在垃圾回收器回收对象之前会先调用该方法，可以做一些操作

##### 3.int与Integer有什么区别
- int:是基本数据类型
- Integer:是引用类型，可以实例化做对象方面的操作

##### 4.重载与重写的区别
- 重载:方法名相同，参数的类型，个数，及返回值不同
- 重写:重写父类的的方法

##### 5.抽象类和接口有什么区别
- 抽象类:用abstract修饰类，方法，不可以被实例化
- 接口:用interface修饰类，所有方法默认以public abstratc 修饰(也只能用这个修饰)，变量默认public static final(且只能用这个修饰)
- 普通类只能继承一个抽象类，可以继承多个接口
- 抽象类可以提供成员方法的实现细节，而接口只能存在public abstract方法
- 抽象类中的成员变量可以是各种类型的，而接口中的成员变量只能是public static final
- 接口中不能含有静态代码块以及静态方法，而抽象类可以有静态代码块和静态方法

##### 6.反射的用途及实现
- 用途:因为反射的代价较高，所以一般用于框架里，使框架变得更加的通用,动态的获取自身的信息，并且可以操作类和对象的内部属性和方法
- 用法:Class
    
```
@org.junit.Test
public void test(){
    //三种使用
    // 类名.class
    Class clazz= Person.class;
    // 对象.getClass()
    Class clazz2=new Person().getClass();
    Class clazz3=null;
    try {
        // 全路径 加载数据库驱动常用
        clazz3=Class.forName("com.zx.mes.reflect.Person");
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    }
    logger.info(JSON.toJSONStringWithDateFormat(clazz,"yyyy-MM-dd HH:mm:ss"));
    logger.info(JSON.toJSONStringWithDateFormat(clazz2,"yyyy-MM-dd HH:mm:ss"));
    logger.info(JSON.toJSONStringWithDateFormat(clazz3,"yyyy-MM-dd HH:mm:ss"));
}
```

##### 7.自定义注解的场景及实现
> 参考java文件夹下的 注解.md

##### 8.HTTP请求的GET与POST方法的区别
- GET在浏览器回退时是无害的(因为只获取数据),POST再次提交时会修改数据
- GET请求会被浏览器主动cache,而POST不会，除非手动设置
- GET请求只能进行url编码，而POST支持多种编码方式
- GET请求在URL中传送的参数是有长度限制的，而POST没有
- 对参数的数据头型，GET只接受ASCALL字符，而POST没有限制
- GET比POST更不安全，因为参数直接暴露在URL上(POST存放在Request body中)，所以不能用来传递敏感信息
- ==本质上POST与GET都是遵循TCP/IP协议编写的，根据不同的需求贴上不同的标签==
- ==GET产生一个TCP数据包，POST产生两个数据包,当然也有坑:==
    - GET与POST都有自己的语义，不能随便混用
    - 在网络环境好的情况下，发一次包的时间和发两次包的时间差别基本可以无视。而在网络环境差的情况下，两次包的TCP在验证数据包完整性上，有非常大的优点
    - 并不是所有浏览器都会在POST中发送两次包，Firefox就只发送一次
##### 9.session与cookie区别
==需要实际测试==
Cookie | Session
---|---
存储在客户端 | 存储在服务器端
两种类型(有生命周期，无生命周期) | 两种实现方式(依赖于cookie,url重写)
cookie中如果设置了路径参数,那么同一个网站中不同路径下的cookie互相是访问不到的.cookie只能是子路径访问父路径设置的cookie | session不能区分路径,同一个用户在访问一个网站期间,所有的session在任何一个地方都可以访问到
不可靠 | 可靠

##### 10.session分布式处理

方式 | 说明 |优点 | 缺点 | 适用场景
---|---|---|--- |---
session黏性 | 用户在访问了某台服务器后，之后的操作就让其只走该服务器就好,nginx配置:| 操作简单，不用对session做任何操作 | 当一台机器挂掉后，流量切向其他的机器。会丢失部分用户的session |发生故障对客户产生的影响较小；服务器发生故障是低概率事件
使用广播的方式 | 当一台服务器中的session中（增删改）了之后，将这个session中的所有数据，通过广播一样的方式，同步到其他的服务器中去。 | 容错性增高 | 机器不能太多，session数量不能太大，否则会造成网络阻塞，是服务器变慢 | 一般不用
中间件共享session | 使用redis或者Memcached去当做有个中间件，session中的数据存放在其中。这里需要的是redis或者Memcached必须是集群,两种做法：  | 黏性：说白了就是，和第一种方式一样，一个用户的请求只走一个服务器并且在拿session数据的时候，都只在该台服务器上，但是用户的session需要保存在redis上，作为备份（容灾用）。当一台服务器挂掉了，那么就可以将该用户的session复制到其他的机器上并且把流量转发 | 非黏性：这种情况下，就是将用户的session存放在redis上，用户在访问的时候，读取修改都在redis上 目前这种做法是大家使用最多的方法|两种做法在优点与缺点里
session存数据库 |  | 数据可以持久化，服务器挂掉了也没关系。 | 用户过多的时候，性能低下 | 不用

##### 11.JDBC流程
> JDBC流程：
- 第一步：加载Driver类，注册数据库驱动；
- 第二步：通过DriverManager,使用url，用户名和密码建立连接(Connection)；
- 第三步：通过Connection，使用sql语句打开Statement对象；
- 第四步：执行语句，将结果返回resultSet；
- 第五步：对结果resultSet进行处理；
- 第六步：倒叙释放资源resultSet-》preparedStatement-》connection。

```
public class JDBCTest {
    /**
     * 数据库相关参数
     */
    //这是驱动名称，此例子中我们加载的是mysql的驱动，在之前需要导入mysql的驱动jar包
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    //连接数据库的url，各个数据库厂商不一样，此处为mysql的;后面是创建的数据库名称
    public static final String JDBC_URL = "jdbc:mysql://localhost:3306/jdbc_test";

    //连接数据库所需账户名
    public static final String JDBC_USERNAME = "root";

    //用户名对应的密码，我的mysql密码是123456
    public static final String JDBC_PASSWORD ="123456";


    public static void main(String[] args) {
        List<Student> students = new ArrayList<Student>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            //第一步：加载Driver类，注册数据库驱动
            Class.forName(JDBC_DRIVER);
            //第二步：通过DriverManager,使用url，用户名和密码建立连接(Connection)
            connection = DriverManager.getConnection(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
            //第三步：通过Connection，使用sql语句打开Statement对象；
            preparedStatement = connection.prepareStatement("select * from student where age =?");
           //传入参数，之所以这样是为了防止sql注入
            preparedStatement.setInt(1, 18);
            //第四步：执行语句，将结果返回resultSet
            resultSet = preparedStatement.executeQuery();
            //第五步：对结果进行处理
            while (resultSet.next()){
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");

                Student student = new Student();
                student.setAge(age);
                student.setName(name);
                students.add(student);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //第六步：倒叙释放资源resultSet-》preparedStatement-》connection
            try {
                if (resultSet!=null && !resultSet.isClosed()){
                    resultSet.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if(preparedStatement!=null && 
                    !preparedStatement.isClosed()){
                    preparedStatement.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                if(connection!=null && connection.isClosed()){
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (Student student:students
             ) {
            System.out.println(student.getName()+"="+student.getAge());
        }

    }
}
```
##### 12.MVC设计思想
> 是一种软件架构的思想，将一个软件按照模型、视图、控制器进行划分。其中，模型用来封装业务逻辑，视图用来实现表示逻辑，控制器用来协调模型与视图(视图要通过控制器来调用模型，模型返回的处理结果也要先交给控制器，由控制器来选择合适的视图来显示 处理结果)。
- 1)模型: 业务逻辑包含了业务数据的加工与处理以及相应的基础服务(为了保证业务逻辑能够正常进行的事务、安全、权限、日志等等的功能模块)
- 2)视图:展现模型处理的结果；另外，还要提供相应的操作界面，方便用户使用。
- 3)控制器:视图发请求给控制器，由控制器来选择相应的模型来处理；模型返回的结果给控制器，由控制器选择合适的视图。
##### 13.equals与 == 的区别 
== 当数据是基本的数据类型时，比较的是值，当是引用类型时，比较的对象的地址
equals 具体要看equals的重写状况来判断
### 集合

##### 14.List和Set区别
- List与Set都继承自Collection接口
- List:元素放入有顺序，元素可重复
- Set:元素放入无顺序，元素不可重复,虽无序，但位置由于该元素的HashCode决定，其位置是固定的
- Set:检索元素的效率低下，删除和插入的效率高，且插入删除不会引起元素位置的改变
- List: 和数组类似，List可以动态增长，查找元素效率高，插入删除元素效率低，会引起其它元素位置改变

##### 15.List和Map区别
参照上面14，Map不继承Collection接口
Map适合存储键值对的数据
##### 16.ArrayList与LinkedList区别
涉及类似数组与链表
##### 17.HashMap和HashTable区别
涉及线程安全问题
##### 18.HashSet和HashMap区别

HashMap | HashSet
---|---
实现了Map接口 | 实现Set接口
存储键值对 | 仅存储对象
调用put()向map中添加元素 | 调用add()方法向Set中添加元素
HashMap使用键（Key）计算Hashcode对|HashSet使用成员对象来计算hashcode值，对于两个对象来说hashcode可能相同，所以equals()方法用来判断对象的相等性,如果两个对象不同的话，那么返回false
HashMap相对于HashSet较快，因为它是使用唯一的键获取对象 | HashSet较HashMap来说比较慢
##### 19.HashMap和ConcurrentHashMap区别
后者为线程安全的，且比HashTable效率要高
##### 20.HashMap的工作原理及代码实现

##### 21.ConcurrentHashMap的工作原理及代码实现

### 线程
##### 22.创建线程的方式及实现
- 一种是继承类Thread

    ```
    /**
     * Thread使用方式
     * @author Created by hyl on 18/2/16.
     * @date 2018-2-16
     */
    public class Thread extends java.lang.Thread{
        @Override
        public void run() {
            System.out.println("sss thread:"+Thread.currentThread().getName());
        }
    }
    ```

- 另一种实现接口Runnable
    ```
    /**
     * Runnable接口
     * @author Created by hyl on 18/2/16.
     * @date 2018-2-16
     */
    public class Runable implements Runnable{
        @Override
        public void run() {
            System.out.println("runnable测试 thread:"+Thread.currentThread().getId());
        }
    }
    ```
##### 23.sleep(),join(),yield()有什么区别
- sleep()方法需要指定等待的时间，它可以让当前正在执行的线程在指定的时间内暂停执行，进入阻塞状态，该方法既可以让其他同优先级或者高优先级的线程得到执行的机会,但是sleep()方法不会释放“锁标志”，也就是说如果有synchronized同步块，其他线程仍然不能访问共享数据
- yield()方法和sleep()方法类似，也不会释放“锁标志”，区别在于，它没有参数，即yield()方法只是使当前线程重新回到可执行状态，所以执行yield()的线程有可能在进入到可执行状态后马上又被执行，另外yield()方法只能使同优先级或者高优先级的线程得到执行机会，这也和sleep()方法不同
- join()方法会使当前线程==等待调用join()方法的线程结束后才能继续执行==

##### 24.CountDownLatch原理
[CountDownLatch](../../../md/java/线程/CountDownLatch.md)
##### 25.CyclicBarrier原理
[CyclicBarrier](../../../md/java/线程/CyclicBarrier.md)
##### 26.Semaphore原理
[Semaphore](../../../md/java/线程/Semaphore.md)  **示例有问题**
##### 27.Exchanger原理
[Exchanger](../../../md/java/线程/Exchanger.md)
##### 28.CoutnDownLatch与CyclicBarrier区别
<table>
<tr><td>CountDownLatch</td><td>CyclicBarrier</td></tr>
<tr><td>减计数方式</td><td>加计数方式</td></tr>
<tr><td>计算为0时释放所有等待的线程</td><td>计数达到指定值时释放所有等待线程</td></tr>
<tr><td>不可重复利用</td><td>可重复利用</td></tr>
</table>

##### 29.ThreadLocal原理分析
[ThreadLocal原理分析](../../../md/java/线程/ThreadLocal.md)
##### 30.线程池的实现原理
[线程池的实现原理](../../../md/java/线程/线程池原理.md)
##### 31.线程池的几种方式
- 1.SingleThreadExecutor：单个后台线程 (其缓冲队列是无界的)
- 2.FixedThreadPool：只有核心线程的线程池,大小固定 (其缓冲队列是无界的)
    - 创建固定大小的线程池。每次提交一个任务就创建一个线程，直到线程达到线程池的最大大小。线程池的大小一旦达到最大值就会保持不变
    - 如果某个线程因为执行异常而结束，那么线程池会补充一个新线程 
- 3.CachedThreadPool：无界线程池，可以进行自动线程回收(一般不用，当所创建的线程都处于不可用时，有可能发生OOM，当然可重写此方法) 
- 4.ScheduledThreadPool：核心线程池固定，大小无限的线程池。此线程池支持定时以及周期性执行任务的需求
##### 32.线程的生命周期
![线程的生命周期](../../../uploads/images/多线程/线程生命周期.jpg)
##### 33.线程安全问题
多个线程对同一资源的修改，导致数据不一致性
##### 34.volatile实现原理

##### 35.synchronized实现原理

##### 36.synchronized与lock的区别

##### 37.CAS乐观锁

##### 38.ABA问题

##### 39.乐观锁的业务场景及实现方式 
## 核心篇
### 数据存储
##### 40.MySQL索引使用的注意事项

##### 41.反模式设计

##### 42.分库与分表设计

##### 43.分库与分表带来的分布式困境与应对之策

##### 44.SQL的优化之道

##### 45.MySQL遇到的死锁问题

##### 46.存储引擎的InnoDB与MyISAM

##### 47.数据库索引的原理

##### 48.为何要用B-Tree

##### 49.聚集索引与非聚集索引的区别

##### 50.limit 20000加载很慢如何解决

##### 51.选择合适的分布式主键方案

##### 52.选择合适的数据存储方案

##### 53.ObjectId规则

##### 54.MongoDB使用场景

##### 55.倒排索引

##### 56.ElasticSearch使用场景





