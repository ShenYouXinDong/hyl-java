### java基础总结
> java常见的8种数据和引用类型，基本运算符，条件分支语句，面向接口编程，常见的集合类，IO操作，基本的网络通信等

#### 1. 8种基本数据

整形 | 浮点型 | 字符型 |布尔类型
---|------|------|---
byte short int long | float double | char | boolen


数据类型 | 字节 | 二进制位数 | 描述
---|---|---|---
byte   | 1  | 8  |
short  | 2  | 16 |
int    | 4  | 32 |
long   | 8  | 64 |
float  | 4  | 32 |
double | 8  | 64 |
char   | 2  | 16 |一个字符可以存储一个汉字
boolean| 1/8| 1  | 

- 数据类型自动转换
    -  int -> long  
    -  byte -> short 
    -  float ->double
    -  int -> float


#### 2. 逻辑运算符
 & | ^ ! && ||

- ==逻辑运算符除了!外都是用于连接两个boolean类型表达式==
- &:只有两边都为true结果是true,否则就是false
- |:只有两边都为false结果是false,否则就是true
- ^:异或:
    - 两边结果一样，就为false
    - 两边结果不一样，就为true
- 
- & 和 && 区别:&无论左边结果是什么，右边都参与运算
- &&:短路与，如果左边为false，那么右边不参与运算
- |与 ||区别:|:无论左边结果是什么，右边都参与运算
- ||短路或，如果左边为true，那么右边不参与运算
- 
- ^ 异或 >> 右移  << 左移
    
```java
    @org.junit.Test
    public void test(){
        int a=10;
        int b=20;
        int temp;
        temp=a;
        a = b;
        b = temp;
        System.out.println("b:"+b +"  a:"+a);
    }

    /**
     * 可能会发生超过int 32位的值
     */
    @org.junit.Test
    public void test2(){
        int a=10;
        int b = 20;
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("b:"+b +"  a:"+a);
    }

    @org.junit.Test
    public void test3(){

        int a = 10;
        int b = 20;
        a = a ^ b;
        //b=a^b^b
        b = a ^ b;
        //a=a^b^a
        a = a ^ b;
        System.out.println("b:"+b +"  a:"+a);

    }

    @org.junit.Test
    public void test4(){
        System.out.println("2的2次方(向左移一位)"+ (2<<1));
        System.out.println("2的3次方"+ (2<<2));
        System.out.println("2的4次方"+ (2<<3));
    }
```

#### 3.一个对象创建过程中执行了什么
- 1：先将硬盘上指定位置的Person.class文件加载进内存。
- 
- 2：执行main方法时，在栈内存中开辟了main方法的空间(压栈-进栈)，然后在main方法的栈区分配了一个变量p。
- 
- 3：在堆内存中开辟一个实体空间，分配了一个内存首地址值。new
- 
- 4：在该实体空间中进行属性的空间分配，并进行了默认初始化。
- 
- 5：对空间中的属性进行显示初始化。
- 
- 6：进行实体的构造代码块初始化。
- 
- 7：调用该实体对应的构造函数，进行构造函数初始化。（）
- 
- 8：将首地址赋值给p ，p变量就引用了该实体。(指向了该对象)

#### 4.this
> 哪个对象调用了this所在的函数，this就代表哪个对象，就是哪个对象的引用

- this对象后面跟上.调用的是成员属性和成员方法(一般方法)；

- this对象后面跟上()调用的是本类中的对应参数的构造函数。
- ==注意：用this调用构造函数，必须定义在构造函数的第一行。因为构造函数是用于初始化的，所以初始化动作一定要执行。否则编译失败。==
- super()和this()是否可以同时出现的构造函数中？
    - 两个语句只能有一个定义在第一行，所以只能出现其中一个。
    - super()或者this():为什么一定要定义在第一行？
    - 因为super()或者this()都是调用构造函数，构造函数用于初始化，所以初始化的动作要先完成。

#### 5.static变量

1、static变量

　按照是否静态的对类成员变量进行分类可分两种：一种是被static修饰的变量，叫静态变量或类变量；另一种是没有被static修饰的变量，叫实例变量。两者的区别是：

　对于静态变量在内存中只有一个拷贝（节省内存），JVM只为静态分配一次内存，在加载类的过程中完成静态变量的内存分配，可用类名直接访问（方便），当然也可以通过对象来访问（但是这是不推荐的）。

　对于实例变量，每创建一个实例，就会为实例变量分配一次内存，实例变量可以在内存中有多个拷贝，互不影响（灵活）。

2、静态方法

　静态方法可以直接通过类名调用，任何的实例也都可以调用，因此静态方法中不能用this和super关键字，不能直接访问所属类的实例变量和实例方法(就是不带static的成员变量和成员成员方法)，只能访问所属类的静态成员变量和成员方法。因为实例成员与特定的对象关联！

　因为static方法独立于任何实例，因此static方法必须被实现，而不能是抽象的abstract。

3、static代码块

　static代码块也叫静态代码块，是在类中独立于类成员的static语句块，可以有多个，位置可以随便放，它不在任何的方法体内，JVM加载类时会执行这些静态的代码块，如果static代码块有多个，JVM将按照它们在类中出现的先后顺序依次执行它们，每个代码块只会被执行一次。

4、static和final一块用表示

static final用来修饰成员变量和成员方法，可简单理解为'全局常量'！

#### 6.抽象类: abstract

抽象类的特点：

1：抽象方法只能定义在抽象类中，抽象类和抽象方法必须由abstract关键字修饰（可以描述类和方法，不可以描述变量）。

2：抽象方法只定义方法声明，并不定义方法实现。

3：抽象类不可以被创建对象(实例化)。

4：只有通过子类继承抽象类并覆盖了抽象类中的所有抽象方法后，该子类才可以实例化。否则，该子类还是一个抽象类。

抽象类的细节：
- 1：抽象类中是否有构造函数？有，用于给子类对象进行初始化。
- 2：抽象类中是否可以定义非抽象方法？
-     可以。其实，抽象类和一般类没有太大的区别，都是在描述事物，只不过抽象类在描述事物时，有些功能不具体。所以抽象类和一般类在定义上，都是需要定义属性和行为的。只不过，比一般类多了一个抽象函数。而且比一般类少了一个创建对象的部分。
- 3：抽象关键字abstract和哪些不可以共存？final ,    private , static
- 4：抽象类中可不可以不定义抽象方法？可以。抽象方法目的仅仅为了不让该类创建对象。

#### 7.接 口
- 1：是用关键字interface定义的。
- 2：接口中包含的成员，最常见的有全局常量、抽象方法。
- 注意：接口中的成员都有固定的修饰符。

    - 成员变量：public static final
    - 成员方法：public abstract

```
interface Inter{

    public static final int x = 3;

    public abstract void show();

}
```

抽象类与接口：

- 抽象类：一般用于描述一个体系单元，将一组共性内容进行抽取，特点：可以在类中定义抽象内容让子类实现，可以定义非抽象内容让子类直接使用。它里面定义的都是一些体系中的基本内容。
- 接口：一般用于定义对象的扩展功能，是在继承之外还需这个对象具备的一些功能。
- 抽象类和接口的共性：都是不断向上抽取的结果。
- 抽象类和接口的区别：
    - 1：抽象类只能被继承，而且只能单继承。
        - 接口需要被实现，而且可以多实现。
    - 2：抽象类中可以定义非抽象方法，子类可以直接继承使用。
        - 接口中都是抽象方法，需要子类去实现。
    - 3：抽象类使用的是 is a 关系。
        - 接口使用的 like a 关系。
    - 4：抽象类的成员修饰符可以自定义。
        - 接口中的成员修饰符是固定的。全都是public的。

#### 8.多 态
多态的好处：提高了程序的扩展性。继承的父类或接口一般是类库中的东西，（如果要修改某个方法的具体实现方式）只有通过子类去覆写要改变的某一个方法，这样在通过将父类的应用指向子类的实例去调用覆写过的方法就行了！

多态的弊端：当父类引用指向子类对象时，虽然提高了扩展性，但是只能访问父类中具备的方法，不可以访问子类中特有的方法。(前期不能使用后期产生的功能，即访问的局限性)

多态的前提：

    1：必须要有关系，比如继承、或者实现。

    2：通常会有覆盖操作。

如果想用子类对象的特有方法，如何判断对象是哪个具体的子类类型呢？

可以可以通过一个关键字 instanceof ;//判断对象是否实现了指定的接口或继承了指定的类

格式：对象 instanceof 类型> ，判断一个对象是否所属于指定的类型。

```
Student instanceof Person = true;//student继承了person类
```

#### 9.匿名内部类
匿名内部类的格式：new 父类名&接口名(){ 定义子类成员或者覆盖父类方法 }.方法。

匿名内部类的使用场景：

当函数的参数是接口类型引用时，如果接口中的方法不超过3个。可以通过匿名内部类来完成参数的传递。

其实就是在创建匿名内部类时，该类中的封装的方法不要过多，最好两个或者两个以内。

实例：

```
new Object(){
    void show(){
        System.out.println('show run');                
    }
}.show();                                    //写法和编译都没问题
```

```
Object obj = new Object(){
    void show(){
        System.out.println('show run');
    }
};
 obj.show();                                //写法正确，编译会报错
```
写法都是正确，1和2都是在通过匿名内部类建立一个Object类的子类对象。

        区别：

        第一个可是编译通过，并运行。

        第二个编译失败，因为匿名内部类是一个子类对象，当用Object的obj引用指向时，就被提升为了Object类型，而编译时会检查Object类中是否有show方法，此时编译失败。
        
#### 10.多线程
写一个延迟加载的单例模式？写懒汉式；当出现多线程访问时怎么解决？加同步，解决安全问题；效率高吗？不高；怎样解决？通过双重判断的形式解决

```

/**
 * 双重锁单例模式
 * @author hyl
 * @date 2018-3-28
 */
public class Singleton3 {

    private static volatile Singleton3 instance;

    private Singleton3(){}

    public static Singleton3 getInstance(){
        if (instance == null) {
            synchronized (Singleton3.class){
                if (instance == null) {
                    instance = new Singleton3();
                }
            }
        }
        return instance;
    }
}

```

11.通用集合操作

```
@org.junit.Test
public void test5(){

    Map<String, String> map = new HashMap<String, String>(10);
    map.put("1", "xxx1");
    map.put("2", "xxx2");
    map.put("3", "xxx3");
    map.put("4", "xxx4");

    //集合通用获取方法
    Set<String> keySet = map.keySet();
    Iterator<String> iterator = keySet.iterator();
    while (iterator.hasNext()) {
        String key = iterator.next();
        System.out.println("key:"+key);
        System.out.println(map.get(key));
    }
    System.out.println("-----------------------------------------------------");
    Set<Map.Entry<String, String>> entrySet = map.entrySet();
    Iterator<Map.Entry<String, String>> entryIterator = entrySet.iterator();
    while (entryIterator.hasNext()) {
        Map.Entry<String, String> entry = entryIterator.next();
        System.out.println(entry.getKey()+"  value:"+entry.getValue());
    }
}
```

将非同步集合转成同步集合的方法：Collections中的 XXX 

```
synchronizedXXX(XXX);

List synchronizedList(list);

Map synchronizedMap(map);

public static Map synchronizedMap(Map m) {

return new SynchronizedMap(m);
```
原理：定义一个类，将集合所有的方法加同一把锁后返回

```
List list = Collections.synchronizedList(new ArrayList());

Map synmap = Collections.synchronizedMap(map);
```
Collection 和 Collections的区别：

Collections是个java.util下的类，是针对集合类的一个工具类,提供一系列静态方法,实现对集合的查找、排序、替换、线程安全化（将非同步的集合转换成同步的）等操作。

Collection是个java.util下的接口，它是各种集合结构的父接口，继承于它的接口主要有Set和List,提供了关于集合的一些操作,如插入、删除、判断一个元素是否其成员、遍历等。


http://www.runoob.com/java/java-object-classes.html






