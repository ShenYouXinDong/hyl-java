#### 1.java基本类的定义
- 1.包名：实际上既是限定，也是路径
- 2.包名与类名的命名：实际上以26个英文字母来命名，驼峰式命名方式 ClassName(方法,变量名是以className)

#### 2.接口

```
/**
 * person接口
 * @author hyl
 * @date 2018-4-4
 */
public interface Person {

    //成员变量：public static final(默认)
    String PERSON_NAME="hyl";
    /**
     * 人会说话
     * 成员方法：public abstract(默认)
     * @param content
     */
    void say(String content);
}

```

```
import com.zx.mes.test.hyl.base.Person;

public class OldMan implements Person{
//8种数据类型的一种，其它类似
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String name;
//只在该类所在包中可见
    int sex;
//    包内所有类中均可见（且可调用），包外有继承关系的子类可见（仅可在子类内部调用）
    protected String xxx;

    public void say(String content) {
        System.out.println("say:" +content);
    }
}

```

```
@Test
    public void test(){
//        Person person = new OldMan();
        OldMan oldMan = new OldMan();
        oldMan.setAge(10);
        oldMan.name = "神一般的促进会";
        System.out.println(oldMan.name);
        System.out.println(oldMan.getAge());
    }

    /**
     * & 左右两边都会执行
     * && 短路与 如果左边false，右边不会执行(这是一个特性 )
     */
    @Test
    public void test2(){
        int sum = 10;
        sum = sum + 10;
        boolean bool = false;
        System.out.println("^"+(2^3));
        if (1==1 & (++sum>20)) {
            System.out.println(sum);
        }
    }

    @Test
    public void test4(){
        int sum = 20;
        if (false & (++sum>20)) {
            System.out.println(sum);//会不会输出，如果输出这个值是多少
        }
        System.out.println(sum);
    }

    @Test
    public void test3(){
        int sum = 20;
        if (false && (++sum>20)) {
            System.out.println(sum);//会不会输出，如果输出这个值是多少
        }
        System.out.println(sum);
    }
```
#### 3. 多态

```
    @Test
    public void test5(){
//多态
        Ting ting = new Ting();
        ting.say(new OldMan(),"老年人");
        ting.say(new YongMan(),"xxxxx");
//构造函数
        new OldMan().say("老年");
        System.out.println(new OldMan(10, "xxx").getAge());
        new OldMan(10, "xxx").getAge();
    }
```

#### 4.集合
ArrayList四种遍历方式 

```
@Test
public void test(){

    ArrayList<String> list = new ArrayList<String>(10);

    list.add("fssf");
    list.add("ffasdfs");
//通用方法
    for (int i=0;i<list.size();i++) {
        System.out.println(list.get(i));
    }

    for (String index : list) {
        System.out.println(" xxx: "+index);
    }

    ListIterator iterator= list.listIterator();
    Iterator iterator1= list.iterator();
// 可以向前，也可以向后
    while (iterator.hasNext()) {
        System.out.println(iterator.next());
    }
//只能向合
    while (iterator1.hasNext()) {
        System.out.println(iterator1.next());
    }
}
```
HashMap

```
@Test
public void test6(){
    Map<String, String> map = new HashMap<String, String>(10);
    map.put("1", "zzz");
    map.put("2", "xxxx");
    map.put("3", "gfg");
    //按键值删除
    map.remove("1");
    map.replace("2", "yyyyy");
    //第一种遍历方式
    for (String key : map.keySet()) {
        System.out.println("key:"+key);
        System.out.println("value:"+map.get(key));
    }

    //第二种
    Set<Map.Entry<String, String>> entrySet = map.entrySet();

    for (Map.Entry<String, String> entry : entrySet) {
        System.out.println("key:"+entry.getKey()+"value:"+entry.getValue());
    }

    //第三种
    Iterator<String> iterator = map.keySet().iterator();
    while (iterator.hasNext()) {
        System.out.println("iterator:key:"+iterator.next());
    }
}
```




    