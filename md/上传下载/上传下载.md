##### 1.spring cloud上传下载(实现网盘功能)
**1.上传中文名称的文件时,会有乱码且不会成功** 

解决
```
zuul:
   routes:
      oss-api:
      path: /oss/**
      serviceId: oss-service
 
localhost:5000/oss/file/upload
这时如果出现中文文件名，上传文件的文件名会出现失败。按照上述大神的办法，直接在这个uri，前面加上”/zuul”，那么请求地址如下：
localhost:5000/zuul/oss/file/upload
```