##### 1.spring boot 配置读取
> 以下分为四种:Environment,@Value 注解方式,PropertiesFactoryBean,瘵配置信息映射成实体类

**代码位置com.hyl.test.controller.EnvironmentController** 


结果:2018-05-08 16:32:11.860  INFO 10116 --- [nio-8080-exec-1] c.h.t.controller.EnvironmentController   : hyl-java

@Value 注解方式有用过不作记录

实体类映射ConfigurationProperties (1.4以后不再支持location这个参数，官网推荐Environment),</br>但仍可映射application.yml中的配置属性

````
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
````

 ![官网解释](../../uploads/images/spring-boot/配置文件映射.PNG)

